Entity Query Decorator module for Drupal 8.x.
This module adds a decorator class around the Entity Query.

REQUIREMENTS
------------
* None.

INSTALLATION INSTRUCTIONS
-------------------------
1.  Enable module and rebuild cache.

NOTES
-----
See the documentation page for usage: https://www.drupal.org/sandbox/chadpeppers/2900464
This module is actively being maintained and updated. Expect more functionality in the future.
